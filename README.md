# Machine Learn Classification - Customer Churn (Kaggle - Telco Customer Churn)

This notebook walks around a standard method to building a model based on **Gradient Boosting Classifier** to predict customer churn with data from Kaggle (*https://www.kaggle.com/datasets/blastchar/telco-customer-churn?resource=download*).

Classification methods with unbalanced datasets usually are plagued with **high accuracy** but low **power**. Most of those usually have good results with **Random Forest Trees**, but statistically a **truncated OLS** can get high power according to few papers in literature, so it might also be worth the shot.

The results here show decent power compared to the type 2 erros I am used to in these classification problem (*almost 3/4 of the Churn are predicted*) but *lower accuracy*. Half of the predicted (*Churn = Yes*) are **false-positives**, but from results it is clear that **buying fidelity is the key**.

The goal here is to show a bit of a *Data Science* pipeline, not an *academic, econometrics-based study*. I am usually very worried/curious about inference, so I tend to gravitate to the later option or both.


## About

I am Leonardo Brioschi, a *Ph.D. candidate at Accounting/Finance track*. Please visit **https://leobrioschi.gitlab.io**


## License

MIT License